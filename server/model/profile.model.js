const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const profileSchema = new Schema({
  destination: String,
  name: String,
  description: String,
  dateOfBirth: {
    type: Date,
    default: Date.now,
  },
  nationality: String,
  language: String,
  phone: String,
  email: String,
  street: String,
  city: String,
  postal: String,
  country: String,
  facebook: String,
  twitter: String,
  github: String,
  linkedin: String,
  instagram: String,
  dribbble: String,
  wordpress: String,
  whatsapp: String,
  skype: String,
  freelance: String,
});

const Profile = mongoose.model("Profile", profileSchema);

module.exports = Profile;
// https://docs.mongodb.com/manual/reference/bson-types/
