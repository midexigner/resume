const mongoose = require("mongoose");
const { isEmail } = require('validator');
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    username:{
        type:String,
        required: [true, 'Please enter an username'],
        unique:true,
        trim:true,
        minlength:3
    },
    email:{
        type:String,
        required: [true, 'Please enter an email'],
        unique: true,
        max:255,
        min:10
    },
    password:{
        type:String,
        max:1024,
        required: [true, 'Please enter a password'],
        minlength: [6, 'Minimum password length is 6 characters'],
    },
    date:{
      type: Date,
      default:Date.now
    }
});

// fire a function before doc saved to db
userSchema.pre('save', async function(next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    next();
  });
  
  // static method to login user
  userSchema.statics.login = async function(email, password) {
    const user = await this.findOne({ email });
    if (user) {
      const auth = await bcrypt.compare(password, user.password);
      if (auth) {
        return user;
      }
      throw Error('incorrect password');
    }
    throw Error('incorrect email');
  };

const User = mongoose.model('User', userSchema);

module.exports = User;