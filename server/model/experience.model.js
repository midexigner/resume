const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const experienceSchema = new Schema({
    username:{ 
        type: String, 
        required: true 
    },
    description: { 
        type: String, 
        required: true
     },
    duration: { 
        type: Number, 
        required: true
     },
    date:{
        type: Date,
        default:Date.now
    } 
});

const Experience = mongoose.model('Experience', experienceSchema);

module.exports = Experience;