const router = require('express').Router();
const profileController = require('../controllers/profileController')

router.get('/',profileController.profile_listing);

// add profile
  router.route('/add').post((req, res) => {
   
    const newProfile = new Profile({
            destination:req.body.destination,
            name:req.body.name,
            description:req.body.description,
            dateOfBirth:req.body.dateOfBirth,
            nationality:req.body.nationality,
            language:req.body.language,
            phone:req.body.phone,
            email:req.body.email,
            street:req.body.street,
            city:req.body.city,
            postal:req.body.postal,
            country:req.body.country,
            facebook:req.body.facebook,
            twitter:req.body.twitter,
            github:req.body.github,
            linkedin:req.body.linkedin,
            instagram:req.body.instagram,
            dribbble:req.body.dribbble,
            wordpress:req.body.wordpress,
            whatsapp:req.body.whatsapp,
            skype: req.body.skype,
            freelance: req.body.freelance,
    })
    
    newProfile.save()
      .then(() => res.json('Profile added!'))
      .catch(err => res.status(400).json('Error: ' + err));
  });

  //   Profile Delete
  router.route('/:id').delete((req, res) => {
    Profile.findByIdAndDelete(req.params.id)
      .then(() => res.json('Profile deleted.'))
      .catch(err => res.status(400).json('Error: ' + err));
  });
  

//   Profile update
  router.route('/update/:id').post((req, res) => {
    Profile.findById(req.params.id)
      .then(prof => {
        prof.destination=req.body.destination;
        prof.name=req.body.name,
        prof.description=req.body.description,
        prof.dateOfBirth=req.body.dateOfBirth,
        prof.nationality=req.body.nationality,
        prof.phone=req.body.phone,
        prof.email=req.body.email,
        prof.street=req.body.street,
        prof.city=req.body.city,
        prof.postal=req.body.postal,
        prof.country=req.body.country,
        prof.facebook=req.body.facebook,
        prof.twitter=req.body.twitter,
        prof.github=req.body.github,
        prof.linkedin=req.body.linkedin,
        prof.instagram=req.body.instagram,
        prof.dribbble=req.body.dribbble,
        prof.wordpress=req.body.wordpress,
        prof.whatsapp=req.body.whatsapp
        prof.save()
          .then(() => res.json('profiles updated!'))
          .catch(err => res.status(400).json('Error: ' + err));
      })
      .catch(err => res.status(400).json('Error: ' + err));
  });

module.exports = router;