const router = require('express').Router();
const authController = require('../controllers/authController');
//  list user
router.get('/',authController.user_listing);
// add User
router.post('/add',authController.register_post);

// Login USer
router.post('/login',authController.login_post);

// Logout User
router.get('/logout',authController.logout_get);

module.exports = router;