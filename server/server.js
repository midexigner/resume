const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const mongoose = require('mongoose');



require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

//  middleware
app.use(cors());
app.use(bodyParser.json());
app.use(cookieParser());

// MongoDB Connected start
const uri = process.env.DB_NAME;
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true,useUnifiedTopology:true }
);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
})
// MongoDB Connected end

app.get("/",(req,res)=>{ res.send("Hello")})

// set the end points for the routes
const profileRouter = require('./routes/profile');
const usersRouter = require('./routes/users');
const { requireAuth, checkUser } = require('./middleware/authMiddleware');

app.get('*', checkUser);
app.use('/profile',profileRouter);
app.use('/users', usersRouter)

app.listen(port,()=>{
    console.log(`Server is running on port: http://127.0.0.1:${port}`);
})