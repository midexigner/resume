[
    {
        "name":"Muhammad Idrees",
        "title":"Developer & Teacher",
        "address":{
            "address1":"123 main street",
            "address2":"123 main street",
            "city":"Grand Rapids",
            "state":"Michigan",
            "postal_code":"49503"
        },
        "topic":["mongoDB","Python","JavaScript","Robots"],
        "employee_number":1234,
        "location":[44.9901,123.0262]
    }
]
