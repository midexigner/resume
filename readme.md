- npx create-react-app .
- npm init -y
- npm install express cors mongoose dotenv validator bcrypt cookie-parser
- npm install -g nodemon


- create `server.js`
```sh
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(bodyParser.json());

app.listen(port,()=>{
   console.log(`Server is running on port: http://127.0.0.1:${port}`);
})

```